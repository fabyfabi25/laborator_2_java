package com.company;

import javax.swing.*;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.print("Cate numere doriti sa adaugati?");
        int numar = Integer.parseInt(JOptionPane.showInputDialog(null, "Cate numere doriti sa adaugati?"));
        JOptionPane.showMessageDialog(null, "Ati ales sa adaugati "+numar+" numere");
        int[] Array= new int[20];
        int i;
        for(i=0; i<numar; i++) {
            int x = Integer.parseInt(JOptionPane.showInputDialog("Introduceti numarul cu indexul: " + (i + 1)));
            Array[i] = x;
        }

        int y =0;
        for(i=0; i<numar; i++) {
            if(Array[i] !=0 ) y++;
        }
        int[] Vector = new int[y];
        for(int j=0; j<numar; j++){
            Vector[j]=Array[j];
        }
        JOptionPane.showMessageDialog(null, "Numerele adaugate sunt " + Arrays.toString(Vector));
        int suma=0;
        for(int j=0; j<numar;j++){
            suma = suma + Vector[j];
        }
        JOptionPane.showMessageDialog(null, "Media numerelor aduagate este: "+(suma/numar));
    }
}
